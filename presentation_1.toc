\beamer@endinputifotherversion {3.33pt}
\beamer@sectionintoc {1}{Introduction}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Indoor Geolocation}{4}{0}{1}
\beamer@subsectionintoc {1}{2}{Use Cases}{6}{0}{1}
\beamer@sectionintoc {2}{Wireless Sensor Networks}{7}{0}{2}
\beamer@subsectionintoc {2}{1}{Sensors interaction}{8}{0}{2}
\beamer@subsectionintoc {2}{2}{Positioning Metrics}{11}{0}{2}
\beamer@sectionintoc {3}{Positioning Algorithms}{16}{0}{3}
\beamer@subsectionintoc {3}{1}{Algorithms Evaluation}{17}{0}{3}
\beamer@subsectionintoc {3}{2}{Algorithms Examples}{27}{0}{3}
\beamer@subsectionintoc {3}{3}{iBeacons}{33}{0}{3}
